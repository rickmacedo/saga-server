from django.db import models

# Create your models here.
class Unprocessed(models.Model):
	lista = models.CharField(max_length=255)
	num = models.IntegerField()

class Pending(models.Model):
	lista = models.CharField(max_length=255)
	num = models.IntegerField()
	data = models.DateTimeField(auto_now_add=True)