from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from saga.models import Unprocessed, Pending

import random
import json
import urllib2

# Create your views here.

def home(request):
	return HttpResponse("hello, I love you")


def get_random(request):

	if request.method != "GET":
		return HttpResponse("")

	try:
		msg = Unprocessed.objects.all()[random.randint(0, Unprocessed.objects.count() - 1)]

		pending = Pending(lista=msg.lista, num=msg.num)
		pending.save()

		retorno = { "lista": msg.lista, "num": msg.num }

		msg.delete()

		return HttpResponse(json.dumps(retorno))
	except Exception, e:
		raise e


def popula(request, lista, maxnum):

	maxnum = int(maxnum)

	for i in range(1, maxnum):
		nova = Unprocessed(lista=lista, num=i)
		nova.save()

	return HttpResponse("pronto!")


@csrf_exempt
def carrega(request):

	if request.method != "POST":
		return HttpResponse("")

	dados = json.loads(request.body)

	lista = dados['lista']
	num = int(dados['num'])
	assunto = dados['assunto']
	remetente = dados['remetente']
	data = dados['data']
	html = dados['html']
	text = dados['text']
	fullhtml = dados['fullhtml']

	try:
		pending = Pending.objects.get(lista=lista, num=num)
		pending.delete()
	except Exception, e:
		pass

	conteudo = {
		"lista": lista,
		"num": num,
		"assunto": assunto,
		"remetente": remetente,
		"data": data,
		"html": html,
		"text": text,
		"fullhtml": fullhtml
	}

	u = urllib2.urlopen('http://localhost:9200/mensagens/' + lista + '/' + str(num), json.dumps(conteudo))
	retorno = u.read()
	u.close()

	return HttpResponse(retorno)




# 	assunto = h.unescape(subject['data-subject']).replace("\t", "")
# remetente = h.unescape(author['data-author']).replace("\t", "")
# msgnum = int(h.unescape(author['data-msgid']))
# data = h.unescape(date.text)
# conteudo_html = content
# text = strip_tags(str(content))