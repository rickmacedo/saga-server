from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'sagasrv.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'saga.views.home'),
    url(r'^pega/', 'saga.views.get_random'),
    url(r'^popula/(?P<lista>\w+)/(?P<maxnum>\d+)', 'saga.views.popula'),
    url(r'^carrega/', 'saga.views.carrega')
)
